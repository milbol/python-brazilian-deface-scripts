#########################################################################
# Thanks to: Kamyar, Gomputor and all stackoverflow community           #
# Greetz to: Amazon Underground and Los Bandoleros                      #
# Created by MixLoL (Amuth)                                             #
#########################################################################

import requests
import datetime

print """\nPrimeira vez usando?, aperte 1 entao, caso contrario, aperte 0 para "limpar" (observacoes) o .txt com suas shells"""

try:
  pergunta = input("Are you an one or a zero? (1/0): ")

  if pergunta == 1:
    shells_nrespondendo = [] 
    shells_funfando = []
    shells_apagadas = []
    with open("b.txt", "r") as arquivo:  #Mude o nome do arquivo.
      for linhas1 in arquivo:
        try:
		  #Ajuste o tempo de resposta do servidor da shell, ta em segundos
          url_shells = requests.get(linhas1.strip(), timeout = 3.000)           
          if url_shells.status_code != 404:
            shells_funfando.append(linhas1)
          elif url_shells.status_code == 404:
            shells_apagadas.append(linhas1)
        except requests.exceptions.Timeout: 
		#Caso ocorra algum erro que o except nao pegue, favor, add vc mesmo e/ou mande o erro 
		#para no repositorio: https://bitbucket.org/mixlol/amuth_toolkit/issues
	          linhas1 += "    n ta respondendo"                       
	          shells_nrespondendo.append(linhas1)
        except requests.exceptions.SSLError:
            linhas1 += "    Erro de SSL"
            shells_nrespondendo.append(linhas1)
        except KeyboardInterrupt:
            print "Ok... saindo"

    with open("b.txt", "w") as arquivo:
        arquivo.truncate()
        shells_totais = shells_nrespondendo + shells_funfando
        nova_lista_shellstotais = []
        contador = 0
        while contador < len(shells_totais):
          variavelqualquer = shells_totais[contador].replace("\n","")
          variavelqualquer += "\n"
          nova_lista_shellstotais.append(variavelqualquer)
          contador += 1
        numero_de_shells_arquivo = len(shells_nrespondendo) + len(shells_apagadas) + len(shells_funfando)
        numero_de_shells_funfando = len(shells_nrespondendo) + len(shells_funfando)
        numero_de_shells_apagadas = len(shells_apagadas)
        texto_noarquivo_sobreashells = "Shells totais: " + str(numero_de_shells_arquivo) + "\t |  " + "Shells funfando: " + str(numero_de_shells_funfando) + "\t |  " + "Shells apagadas: " + str(numero_de_shells_apagadas) + "\n\n\n"
        ultimoupdate = "\n\n\n\n" + "Atualizado em " + datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%Y %H:%M:%S')
        arquivo.writelines(texto_noarquivo_sobreashells)
        arquivo.writelines(nova_lista_shellstotais)
        arquivo.writelines(ultimoupdate)

    if len(shells_apagadas) > 0:
	  #Mude o nome para que o arquivo de shells apagadas seja de sua preferencia
      with open("e.txt", "w") as arquivo_shells_apagadas:                 
        arquivo_shells_apagadas.writelines(shells_apagadas)
  
  

  elif pergunta == 0:
    def replace_all(text, dic):
      for i, j in dic.iteritems():
          text = text.replace(i, j)
      return text

    with open("b.txt", "r") as arquivo:
	    global linhas
	    linhas = []
	    for linhas_vazias in arquivo:
		    linhas2 = linhas_vazias.strip()
		    linhas.append(linhas2)

    with open("b.txt", "w") as msmarquivo:
	    seraoretirados = {"n ta respondendo":"", "Erro de SSL":"", linhas[0]:"", linhas[-1]:""}
	    msmarquivo.truncate()
	    urlpurificadas = []
	    palavras_seraoretiradas = [linhas[0], linhas[-1]]
	    for linha in linhas:
		    if linha != palavras_seraoretiradas:
			    variavelqualquer = replace_all(linha,seraoretirados)
			    urlpurificadas.append(variavelqualquer)
		    else:
			    variavelqualquer3 = replace_all(linha,seraoretirados)
			    urlpurificadas.append(variavelqualquer3)
	
	    novaurls_purificadas = []	
	    contador = 0
	    while contador < len(urlpurificadas):
		    if len(urlpurificadas[contador]) > 1:
			    variavelqualquer2 = urlpurificadas[contador] + "\n"
			    novaurls_purificadas.append(variavelqualquer2)
		    contador += 1
	    msmarquivo.writelines(novaurls_purificadas)

  else:
    print "\nHold on elliot, you are either one or zero"

except KeyboardInterrupt:
  print "\n\nOk... saindo"